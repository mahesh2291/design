import React from 'react'
import { Layout } from 'antd';
import Searchbar from '../Searchbar';
const { Header, Footer, Sider, Content } = Layout;

function Pagelayout() {
    return (
        <div>
            <Layout>
      <Sider><Searchbar /></Sider>
      <Layout>
        <Header><Searchbar /><Searchbar /></Header>
        <Content>Content</Content>
        <Footer>Footer</Footer>
      </Layout>
    </Layout>
        </div>
    )
}

export default Pagelayout
