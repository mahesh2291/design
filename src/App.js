import React from 'react';
import Pagelayout from './Components/Layout/Pagelayout'
import './App.css';

function App() {
  return (
    <div className="App">
      <Pagelayout />
    </div>
  );
}

export default App;
